#Patrycja Lewandowska s176702
from Crypto import Random
from Crypto.Cipher import AES
import random


p=1117
g=5

#ALICJA 
a = random.randint(1 , p-1)
A = (g**a) % p

#EWA
c = random.randint(1 , p-1 )
C = (g**c) % p

#BOB
b = random.randint(1, p-1)
B = (g**b) % p
Bc =(B**c) % p
print(Bc)

#EWA
Cb = (C**b) % p
print(Cb)

d =random.randint(1, p-1)
D = (g**d) % p
Da = (D**a) % p
#ALICJA

Ad = (A**d) % p
print(Da)
print(Ad)

key_AE = (Ad).to_bytes(16, byteorder='big') #klucz alicji i ewy
print(key_AE)
key_EB = (Cb).to_bytes(16, byteorder='big') #klucz boba i ewy
print(key_EB)

def padding(a): #dopelnianie
        return a + b"\0" * (AES.block_size - len(a) % AES.block_size)

def encrypt(message, key): #szyfrowanie
        message = padding(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CFB, iv)
        return iv + cipher.encrypt(message)

def encrypt_file(file_name , number, key):
        with open(file_name, 'rb') as file: #odczyt z pliku
           text = file.read()
        enc = encrypt(text, key)
        if number == 2:
             file_name= "2_tekst_zaszyfrowany_Alicji.txt"
        if number == 4:
             file_name = "4_tekst_zaszyfrowany_przez_Ewe.txt"
        with open(file_name , 'wb') as file: #zapis do pliku
            file.write(enc)
        
def decrypt(ciphertext, key): #deszyfrowanie
        iv = ciphertext[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CFB, iv)
        plaintext = cipher.decrypt(ciphertext[AES.block_size:])
        return plaintext.rstrip(b"\0")

def decrypt_file(file_name, number , key):
        with open(file_name, 'rb') as f: #odczyt z pliku
            ciphertext = f.read()
        dec = decrypt(ciphertext, key)
        if number == 3:
            file_name = "3_tekst_odszyfrowany_przez_Ewe.txt"
        if number == 5:
            file_name = "5_tekst_odszyfrowany_przez_Boba.txt"
        with open(file_name, 'wb') as f: #zapis do pliku
            f.write(dec)

tekst_wejsciowy_Alicji = "1_tekst_wejsciowy_Alicji.txt"
encrypt_file(tekst_wejsciowy_Alicji , 2 , key_AE) 

tekst_do_odszyfrowania_ewa = "2_tekst_zaszyfrowany_Alicji.txt"
decrypt_file(tekst_do_odszyfrowania_ewa,3 , key_AE)

tekst_zaszyfrowany_przez_ewe ="3_tekst_odszyfrowany_przez_Ewe.txt"
encrypt_file(tekst_zaszyfrowany_przez_ewe , 4 , key_EB)

tekst_odszyfrowany_przez_Boba = "4_tekst_zaszyfrowany_przez_Ewe.txt"
decrypt_file(tekst_odszyfrowany_przez_Boba,5 , key_EB)




