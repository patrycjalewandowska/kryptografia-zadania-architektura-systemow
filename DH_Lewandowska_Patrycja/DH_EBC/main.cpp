//PATRYCJA LEWANDOWSKA s176702
#include<iostream>
#include<fstream>
#include "std_lib_facilities.hpp"
#include <bitset>


using namespace std;
string TextToBinaryString(string words) {
    string binaryString = "";
    for (char& _char : words) {
        binaryString +=bitset<8>(_char).to_string();
    }

    return binaryString;
}

void addZeros(string& str, int n)
{
    for (int i = 0; i < n; i++) {
        str = "0" + str;
    }
}


string getXOR(string a, string b)
{


    int aLen = a.length();
    int bLen = b.length();

    if (aLen > bLen) {
        addZeros(b, aLen - bLen);
    }
    else if (bLen > aLen) {
        addZeros(a, bLen - aLen);
    }

    int len = max(aLen, bLen);

    string res = "";
    for (int i = 0; i < len; i++) {
        if (a[i] == b[i])
            res += "0";
        else
            res += "1";
    }

    return res;
}
vector<string> szyfrowanie_odszyfrowanie(vector<string> a, string klucz , string msg ){
    vector<string> wiadomosc;
    string inf;
    cout<<endl<<msg<<endl;

for(int i = 0; i<a.size(); i++)
 {
    inf = getXOR(a[i],klucz);
    wiadomosc.push_back(inf);

    cout<<wiadomosc[i]<<endl;
     }

     return wiadomosc;

}

int wartosc_publicz(int g, int p, int wykladnik ){

  int Y  = fmod(pow(g,wykladnik), p);
        return Y;}

int main()

{ srand(time(NULL));
    int p = 23;

    int g = 5;

//ALICJA
//WYBIERA LOSOWY WYKLADNIK a
int a = 7;
int A  = wartosc_publicz(g,p,a); // ALICJA OBLICZA A I WYSYLA DO BOBA
cout<<"A= "<<A<<endl;

//EWA
//ODRZUCA A I LOSUJE WYKLAD c
int c = 9;//rand()% (p-1) + 1;

//int c = 90;

int C  = wartosc_publicz(g,p,c); //EWA WYSYLA DO BOBA
cout<<"C= "<<C<<endl;


//BOB
//WYBIERA LOSOWY WYKLADNIK b
int b = 6;//rand()% (p-1) + 1;
//BOB MYSLI , ZE OTRZYMAL WIADOMOSC OD ALICJI I OBLICZA  C^b
int Cb = wartosc_publicz(C,p,b); // WSPOLNY KLUCZ BOBA I EWY
int B = wartosc_publicz(g,p,b); // USTALA B i WYSYLA ALICJI
cout<<"B= "<<B<<endl;

//EWA OBLICZA B^c
int Bc = wartosc_publicz(B,p,c); // WSPOLNY KLUCZ BOBA I EWY
//EWA WYBIERA LOSOWY WYKLADNIK d
int d = 10;// rand()% (p-1) + 1;
//EWA OBLICZA A^d

int Ad = wartosc_publicz(A,p,d);// WSPOLNY KLUCZ EWY I ALICJI
int D =  wartosc_publicz(g,p,d);// EWA WYSYLA D DO ALICJI , ALICJA WYSLI ZE TO OD BOBA
cout<<"D= "<<D<<endl;


//ALICJA OBLICZA D^a
int Da = wartosc_publicz(D,p,a); // WSPOLNY KLUCZ EWY I ALICJI


cout<<"klucz alicji i ewy "<<Ad<<endl;
cout<<"klucz ewy i boba "<<Cb<<endl;

string klucz_AE = bitset<4>(Ad).to_string();
string klucz_BE = bitset<4>(Cb).to_string();

string klucz_Alicji_Ewy ="";
string klucz_Boba_Ewy = "";

for(int i =0 ; i<16 ; i++){

    klucz_Alicji_Ewy+=klucz_AE;
    klucz_Boba_Ewy+=klucz_BE;
}
cout<<"klucz Alicji i Ewy: "<<klucz_Alicji_Ewy<<endl<<"klucz Boba i Ewy: "<<klucz_Boba_Ewy<<endl;





    ifstream tekst_wejsciowy_Alicji;
    fstream tekst_zaszyfrowany_Alicji;
    fstream tekst_skradziony;
    fstream tekst_do_Boba;
    fstream tekst_odszyfrowany_przez_Boba;

    string linia;
    vector<char> tekst_alicji_input;
    vector<string> porcja_tekst_alicji;
    vector<string> t_zaszyfrowany_Alicji;
    vector<string> tekst_Aliji_binarnie;



    tekst_wejsciowy_Alicji.open("1_tekst_wejsciowy_Alicji.txt", ios::in);

     while(getline(tekst_wejsciowy_Alicji, linia)){
            for(int i = 0; i < linia.size(); i++){
                tekst_alicji_input.push_back(linia[i]);

                }}
		tekst_wejsciowy_Alicji.close();

		string porcja;
//DZIELENIE ZNAKOW PO 8 I DOPELNIANIE ZERAMI

        int textSize = tekst_alicji_input.size();
        int zerosCount = 8 - (textSize % 8);
        if(textSize%8 != 0){
        for(int i = 0; i < zerosCount; i++){
            tekst_alicji_input.push_back('0');
        }}

        for(int i =0 ; i<tekst_alicji_input.size(); i++)
		{
		    porcja += tekst_alicji_input[i];

		    if((i+1)%8 == 0 ){
            porcja_tekst_alicji.push_back(porcja);
            porcja = "";
            }
		}

		for(int i = 0; i < porcja_tekst_alicji.size(); i++){
            cout << "Linia " << i << " " << porcja_tekst_alicji[i] << endl;
		}


		cout<<endl<<"wiadomosc Alicji binarnie"<<endl;

		for( int i = 0 ; i<porcja_tekst_alicji.size() ; i++){

            cout<<"linia "<<i<<" "<<(TextToBinaryString(porcja_tekst_alicji[i]))<<endl;
           tekst_Aliji_binarnie.push_back(TextToBinaryString(porcja_tekst_alicji[i]));}

string szyfrogram;

    t_zaszyfrowany_Alicji = szyfrowanie_odszyfrowanie(tekst_Aliji_binarnie,klucz_Alicji_Ewy ,"wiadomosc Alicji zaszyfrowana");

     tekst_zaszyfrowany_Alicji.open("2_tekst_zaszyfrowany_Alicji.txt", ios::out);

    for(int i =0; i<t_zaszyfrowany_Alicji.size() ; i++){


 istringstream in(t_zaszyfrowany_Alicji[i]);
    bitset<8> bin_to_string;
    while(in >> bin_to_string )
       tekst_zaszyfrowany_Alicji<< char(bin_to_string.to_ulong());}

    tekst_zaszyfrowany_Alicji.close();

 //ALICJA MYSLI ZE WYSYLA WIADOMOSC DO BOBA, ALE TO EWA PRZECHWYTUJE

 string wiadomosc_do_B;
 vector<string> wiad_Ewa_przechwytuje;

     wiad_Ewa_przechwytuje = szyfrowanie_odszyfrowanie(t_zaszyfrowany_Alicji,klucz_Alicji_Ewy,"Wiadomosc ktora przechwytuje Ewa");

     tekst_skradziony.open("3_tekst_odszyfrowany_przez_Ewe.txt", ios::out);

      for(int i =0; i<wiad_Ewa_przechwytuje.size() ; i++){

        istringstream ins(wiad_Ewa_przechwytuje[i]);

     bitset<8> bin_to_stringa;
     char  znak;
     while(ins >> bin_to_stringa ){

         znak = char(bin_to_stringa.to_ulong());
        if(( znak != '0')){
            tekst_skradziony <<  znak;
        }}}

    tekst_skradziony.close();

    //EWA SZYFRUJE WIADOMOSC DO BOBA , BOB  MYSLI ZE OD ALICJI
    string wiadomosc_;
    vector<string> vec_widomosc_do_boba;

     vec_widomosc_do_boba = szyfrowanie_odszyfrowanie(wiad_Ewa_przechwytuje,klucz_Boba_Ewy,"wiadomosc do Boba ");

     tekst_do_Boba.open("4_tekst_zaszyfrowany_przez_Ewe.txt", ios::out);

      for(int i =0; i<vec_widomosc_do_boba.size() ; i++){

        istringstream inss(vec_widomosc_do_boba[i]);

     bitset<8> bin_to_stringa;
     while(inss >> bin_to_stringa )
      tekst_do_Boba << char(bin_to_stringa.to_ulong());}

    tekst_do_Boba.close();

    //BOB ODSZYFROWUJE WIADOMOSC OD "ALICJI"

    string Bob_w;
    vector<string> vec_widomoscBoba;

     vec_widomoscBoba = szyfrowanie_odszyfrowanie(vec_widomosc_do_boba , klucz_Boba_Ewy , "wiadomosc odszyfrowana przez Boba ");

     tekst_odszyfrowany_przez_Boba.open("5_tekst_odszyfrowany_przez_Boba.txt", ios::out);

      for(int i =0; i<vec_widomoscBoba.size() ; i++){

        istringstream inss(vec_widomoscBoba[i]);
    char  znak;
     bitset<8> bin_to_stringa;
     while(inss >> bin_to_stringa ){
        znak = char(bin_to_stringa.to_ulong());
        if(( znak != '0')){
            tekst_odszyfrowany_przez_Boba <<  znak;
        }}}
    tekst_odszyfrowany_przez_Boba.close();

 return 0;
}
