#Patrycja Lewandowska s176702

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

Para_klucza = RSA.generate(3072)
#generuje pare n, e , d , gdzie

# e to liczba calkowita - wykladnik publiczny RSA,musi to być nieparzysta liczba dodania. Zazwyczaj jest 
#to niewielka liczba z bardzo małą liczbą w reprezentacji binarnej. Standard FIPS wymaga, aby wykładnik publiczny
# wynosił co najmniej 65537 (domyślnie tyle wynosi).

# n to iloczyn dwoch liczb pierwszych p i q

#d to prywatny wykładnik RSA (klucz prywatny) , dzieki ktoremu bedziemy mogli odkodowac wiadomosc

#Dlugosc klucza musi to być co najmniej 1024, ale zaleca się 2048. Standard FIPS definiuje tylko 1024, 2048 i 3072.


klucz_publiczny = Para_klucza.publickey()
#print(f"Public key:  (n={hex(pubKey.n)}, e={hex(pubKey.e)})") wyswietlanie n i e 

#print(f"Private key: (n={hex(pubKey.n)}, d={hex(keyPair.d)})")   wyswietlanie n i d

file_name = "plik1.txt"
with open(file_name, 'rb') as file: #odczyt z pliku
    text = file.read()

file.close()

encryptor = PKCS1_OAEP.new(klucz_publiczny) #Kluczowy obiekt używany do szyfrowania lub deszyfrowania wiadomości. 
                                             # PKCS1_OAEP to klasa szyfrujaca a new to jej funkcja ktora zwraca obiekt który
                                             #  można użyć do wykonania  szyfrowania lub deszyfrowania.
tekst_zaszyfrowany = encryptor.encrypt(text) #szyfruje wiadomosc
                                    #wartosc przed zaszyfrowaniem jest podniesiona do wykladnika publicznego z tego liczymy
                                    # modulo z modulu n , a wynik tego dzialania jest wartoscia zaszyfrowana

file_name2= "plik2.txt"
with open(file_name2 , 'wb') as f: #zapis do pliku
    f.write(tekst_zaszyfrowany) 
f.close()

with open(file_name2, 'rb') as fil: #odczyt z pliku
    tekst_do_odszyfrowania = fil.read()


decryptor = PKCS1_OAEP.new(Para_klucza) #Odszyfrowanie jest możliwe tylko przy użyciu prywatnego klucza.
odszyfrowany_tekst = decryptor.decrypt(tekst_do_odszyfrowania)
#odszyfrowywanie polega na podniesieciu wartosci zaszyfrowanej do wykladnika 
#prywatnego ( d ) z tego liczby modulo z modułu n , z tego obliczenia powstaje 
#wartosc odszyfrowana

#print('Odszyfrowany:', decrypted)

file_name3 = "plik3.txt"
with open(file_name3, 'wb') as fi: #zapis do pliku
    fi.write(odszyfrowany_tekst)

fi.close()

print("tekst do zaszyfrowania jest w pliku plik1.txt")
print("tekst zaszyfrowany zostal zapisany w plik2.txt")
print("tekst zaszyfrowany zostal odszyfrowany i zapisany w plik3.txt")

#do wykanania tego zadania wykorzystałam dokumentacje PKCS1 OAEP (RSA)
#https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.PKCS1_OAEP-module.html
