#Patrycja Lewandowska s176702
from Crypto import Random
from Crypto.Cipher import AES

key = Random.get_random_bytes(16) #generowanie klucza

def padding(a): #dopelnianie
        return a + b"\0" * (AES.block_size - len(a) % AES.block_size)

def encrypt(message, key): #szyfrowanie
        message = padding(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

def encrypt_file(file_name):
        with open(file_name, 'rb') as file: #odczyt z pliku
           text = file.read()
        enc = encrypt(text, key)
        file_name= "plik2.txt"
        with open(file_name , 'wb') as file: #zapis do pliku
            file.write(enc)
        
def decrypt(ciphertext, key): #deszyfrowanie
        iv = ciphertext[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        plaintext = cipher.decrypt(ciphertext[AES.block_size:])
        return plaintext.rstrip(b"\0")

def decrypt_file(file_name):
        with open(file_name, 'rb') as f: #odczyt z pliku
            ciphertext = f.read()
        dec = decrypt(ciphertext, key)
        file_name = "plik3.txt"
        with open(file_name, 'wb') as f: #zapis do pliku
            f.write(dec)

file_to_encrypt = "plik1.txt"
encrypt_file(file_to_encrypt) #szyfrowanie

file_to_decrypt = "plik2.txt"
decrypt_file(file_to_decrypt)  #odszyfrowanie

#print("klucz: ", key)
print("tekst do zaszyfrowania jest w pliku plik1.txt")
print("tekst zaszyfrowany zostal zapisany w plik2.txt")
print("tekst zaszyfrowany zostal odszyfrowany i zapisany w plik3.txt")