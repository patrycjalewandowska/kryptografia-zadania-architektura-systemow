//Patrycja Lewandowska s176702
//zadanie 1 AiASO /algorytm FCFS

#include<iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

int RandNumber(int g2, int g1 ){
   int a = (rand() % g2) + g1; //losowanie czasow dla waiting time i burst time innych funkcji

    return a;
}

//funkcja liczaca czas oczekiwania
void Waiting_Time_(int bt[],int n, int ct[], int wt[], int at[]){
   wt[0] = 0; //pierwszy proces  bedzie mial czas oczekiwania rowny zero,bo jako pierwszy zacznie sie
        ct[-1] = 0; //zapobiega wyswietlaniu liczby zero jako pierwszego elementu oraz dodatkowo wyswietla ostatni element ct
    for(int i = 1; i < n ; i++){
        ct[i]=ct[i-1]+bt[i]; // completion_time to suma poprzednich czasow burst_time


        wt[i] = ct[i] - at[i];

        if (wt[i] < 0) {wt[i] = 0; }
    }
    // Jeśli czas oczekiwania na proces jest ujemny to znaczy, że jest już w kolejce gotowości
    // zanim procesor stanie się bezczynny, więc jego czas oczekiwania wynosi 0
    }

void TurnAround_Time(int tat[],int bt[],int wt[],int n)
{
    for (int i = 0; i < n ; i++)
        tat[i] = bt[i] + wt[i];

}

void Average(int n,int tab_av[], string inscription){
    int suma = 0;
    for(int i=0;i<n;i++){
        suma=+tab_av[i];
    }

    cout<<inscription<<(float)suma/(float)n<<endl;

}

bool Equality_of_numbers(int number,vector<int> nm){

        for(int i=0; i<nm.size(); i++){
            if(number == nm[i]){
                return true;
            }
        }
    nm.push_back(number);
    return false; //jesli nie znajdziemy liczby ktora juz byla,zapisujemy ja w wektorze i zwracamy falsz

}


int main()
{   srand(time(NULL));

    int n = 30;
    int burst_time[n];
    int arrival_time[n];
    int completion_time[n];
    int waiting_time[n];
    int turnaround_time[n];
    int number = 0;
    bool eq = true;
    vector<int> nm;

    for(int k = 0; k<5; k++){

    do{
        number = RandNumber(89,10); //zakres od 10 do 100
        eq = Equality_of_numbers(number,nm);

    }while(eq == true); //jesli ozstnie znaleziona liczba(prawda) nalezy jeszcze raz wylosowac liczbe ustalajaca zakres



    for( int i = 0; i<30; i++){

        burst_time[i] = RandNumber(number,0);
        arrival_time[i]=RandNumber(30,0);
        completion_time[i]=0;
        waiting_time[i]=0;
        turnaround_time[i]=0;
    }
    arrival_time[0] = 0;// pierwszy proces zacznie się odrazu
    Waiting_Time_(burst_time,n,completion_time,waiting_time,arrival_time);
    TurnAround_Time(turnaround_time,burst_time,waiting_time,n);

    cout<<"Seria "<<k<<"zakres od 10 do "<<number<<endl;

    cout<<"Processes\t"<<"Arrival time\t"<<"Burst Time\t"<<"Completion Time\t\t"<<"Turn Around Time\t"<<"Waiting Time"<<endl;

    for(int i=0;i<n;i++){
        cout<<i+1<<"\t\t"<<arrival_time[i]<<"\t\t"<<burst_time[i]<<"\t\t"<<completion_time[i]<<"\t\t\t"<<turnaround_time[i]<<"\t\t\t"<<waiting_time[i]<<endl;
    }

    Average(n,waiting_time,"average waiting time: ");
    Average(n,completion_time,"average completion time: ");

    cout<<endl<<endl;

    }
    return 0;
}


//ŻRÓDŁA
//Prezentacje AiASO
//https://www.javatpoint.com/os-fcfs-scheduling
