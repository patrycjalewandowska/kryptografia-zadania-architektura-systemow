//Patrycja Lewandowska s176702
#include <iostream>
#include <vector>

using namespace std;

struct Process {
    int p_id; // Process ID
    int p_bt; // Burst Time
    int p_at; // Arrival Time
 };


int main()
{

    //procesy
   Process proc[] = { { 0, 6, 0 }, { 1, 8, 1 },
                    { 2, 7, 3 }, { 3, 3, 2},{ 4, 9, 5 },{ 5, 2, 4 } };

    int n = sizeof(proc) / sizeof(proc[0]);


    int i = 0; // iterator - aktualny czas
    int completed_processes = 0; // zakonczone procesy
    vector<int> vecProcess; //wektor bedzie przechowywal id procesow ktore przybyly i czekaja na swoja kolej
    int copy_bt[n]; // tablica czasow bt

    int average = 0; // srednia czasu oczekiwania
    int finishtime[n];// czas zakonczenia procesu
    int waiting_time[n];
    int tt_time[n]; // Turn around time

    int index; // zmienna przechowujaca id najmniejszego czasu trawania procesu
    int vall_min; // najmniejszy czas trwania procesu w petli

    for(int j = 0; j < n; j++){ // kopiowanie czasu bt
       copy_bt[j] = proc[j].p_bt;
    }

    do{
        for( int h = 0 ; h<n; h++){
            if(proc[h].p_at == i){
                vecProcess.push_back(proc[h].p_id);//wektor który przechowuje id
                break;
            }
        }


        for(int f = 0 ; f<vecProcess.size(); f++){
            if(copy_bt[vecProcess[f]]<vall_min){ //znajdywanie najmnijeszego czasu bt poprzez wektory z id procesorów ktore już przybyły
                vall_min=copy_bt[vecProcess[f]];
                index = f;
            }
        }

        if(copy_bt[vecProcess[index]] == 0){ // czas rowny zero to oznacza ze proces sie zakonczyl
            finishtime[vecProcess[index]] = i; //zapisanie czasu zakoczenie od poczatku startu programu
            vecProcess.erase (vecProcess.begin()+index); // usuniecie id
           completed_processes++;// zakonczony proces
        }
        else if(copy_bt[vecProcess[index]] != 0){
            copy_bt[vecProcess[index]]--; // proces dalej trwa
        }
        else{ cout<<"Niepoprawna wartosc\n";}

        i++; // zwiekszenie aktualnego czasu o jeden

    }while(completed_processes<n);

     cout<<"Processes ID\t"<<"WAITING TIME\t"<<"ARRIVAL TIME"<<"\tBUSRT TIME\t"<<"TURN AROUND TIME\t"<<endl;

    for(int a=0; a<n; a++){
    //liczenie czasu oczekiwania , czasu tt, srednie
    waiting_time[a] = finishtime[a] - proc[a].p_at - proc[a].p_bt;
    tt_time[a] = proc[a].p_bt +  waiting_time[a];
    average += waiting_time[a];
    //wyswietlanie
    cout<<proc[a].p_id<<"\t\t"<<waiting_time[a]<<"\t\t"<<proc[a].p_at<<"\t\t"<<proc[a].p_bt<<"\t\t"<<tt_time[a]<<"\t\t"<<endl;
    }



  cout<<"Average of waiting time: "<<average/n<<endl;

    return 0;
}
