//Patrycja Lewandowska s176702
#include <iostream>
#include<vector>
#include <cstdlib>
#include <ctime>

using namespace std;


struct Memory_cell{
    int id_vec;
    int size_process;
    int memory_size;// zapamietuje ilosc jednostek czasu gdzie proces zostaje wykonany - pomocne przy zwalnianiu procesu

   };



int main()
{
    srand( time( NULL ) );

   Memory_cell process[6] = {{0,200,0},{0,120,0},{0,185,0},{0,462,0},{0,53,0},{0,89,0}}; // 6 przykladowych procesow - jednostki czasu
   vector<int> memory;
   memory.push_back(0); // zero jest na poczatku wektora jako liczba pomocnicza - procesy z indeskem zerowym nie są przydzielone w pamieci
   memory.push_back(2048);
   int size_m;
   int out_process[4];

    int i = 1;// index dla pamieci
    int i_proc = 0; // index dla procesow

    while(i_proc !=6){
//jesli zero jest w wektorze - onzacza to ze w tym indexie jest proces
        if(memory[i] != 0){ // JESLI KOMORKA JEST ROWNA ZERO OZNACZA TO ZE JEST ZAJETA PRZEZ PROCES
            if( process[i_proc].size_process < memory[i] && memory[i]/2 >  process[i_proc].size_process ){
                    size_m = memory[i];
                    memory.erase(  memory.begin() + i);
                    memory.insert( memory.begin() + i ,size_m/2);
                    memory.insert( memory.begin() + (i+1) ,size_m/2);


                    // dzielenie komorek
            }
            else if(process[i_proc].size_process > memory[i]){
                i++;
            }
            else{
                process[i_proc].id_vec = i;
                process[i_proc].memory_size = memory[i];
                memory[i] = 0;
                i_proc++; // KOLEJNY PROCES
                i = 1; // W PAMIECI OD LEWEJ STRONY BEDZIEMY SUZKAC MEIJSCA NA NOWY PROCES

            }
        }
        else {
            i++;
        }
    }


   cout<<"Przed zwolnieniem pamieci"<<endl;
    int index = 0;
    for(int k = 1; k <memory.size(); k++){

        if(memory[k] == 0){
            while(1){

                 if(k != process[index].id_vec){
                    index ++;}
                    else{
                        cout<<"Process "<<process[index].id_vec;
                        break;
                    }
            }
        }else{
        cout<<memory[k];
        }
        cout<<" | ";

    }

//wygenerowane liczby - losowe procesy ktore beda zwalniane z pamieci
   int N = 4;

    int p, b, licznik;
    bool t;



    licznik = 0;
    do
    {
        p =  rand() % 6;

        t = true;
        for(b = 0; b < licznik; b++)
            if(out_process[b] == p)
            {
                t = false;
                break;
            }

        if(t) out_process[licznik++] = p;

    } while(licznik < N);
////////////////////////////////////////////////////////////////////////////////
//zwalnianie procesow
    index = 0;

    for(int a = 0 ; a<4; a++){

        while(1){
            if(out_process[a] == process[index].id_vec){
                memory[out_process[a]] = process[index].memory_size;
                process[index].id_vec = 0;
                index = 0 ;
                break;
            }
            else{
                index++;
            }
        }


    }
    index = 0;
    bool powtarza = false;
    int result;
    while(index != memory.size()){

        if( memory[index] == memory[index+1] ){
            for(int f= 0 ; f<6; f++){ // czy w konkretnej komorce wektora znajduje sie proces
                if(index == process[f].id_vec){
                    index++;
                    powtarza = true;
                    break;
                }
            }
            if(powtarza == true){
                    powtarza = false;
                    continue;}

            result = memory[index];
            memory[index] = result*2;
            memory.erase(memory.begin()+(index+1));

        }
        else{index++;}

    }



    cout<<endl<<"Zwlonione zostaly procesy: ";
    for(i = 0; i < N; i++) cout << out_process[i] << " ";


    cout<<endl<<"Po zwolnieniu pamieci"<<endl;
    for(int k = 1; k <memory.size(); k++){

        if(memory[k] == 0){
            while(1){

                 if(k != process[index].id_vec){
                    index ++;}
                    else{
                        cout<<"Process "<<process[index].id_vec;
                        break;
                    }
            }
        }else{
        cout<<memory[k];
        }
        cout<<" | ";

    }





    return 0;
}
