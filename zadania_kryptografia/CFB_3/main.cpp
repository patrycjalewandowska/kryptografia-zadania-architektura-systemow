//CFB
//PATRYCJA LEWANDOWSKA s176702

#include<iostream>
#include<fstream>
#include "std_lib_facilities.hpp"
#include <bitset>

using namespace std;

//GENEROWANIE KLUCZA O VECTORA POCZATKOWEGO
string generacja(){
        string klucz = "";

    for(int i = 0 ; i<64 ; i++)
    {
        int x = rand()%2;
       string str =to_string(x);
       klucz+=str;

    }

    return klucz;
}
   ;
string TextToBinaryString(string words) {
    string binaryString = "";
    for (char& _char : words) {
        binaryString +=bitset<8>(_char).to_string();
    }

    return binaryString;
}
void addZeros(string& str, int n)
{
    for (int i = 0; i < n; i++) {
        str = "0" + str;
    }
}


string getXOR(string a, string b)
{


    int aLen = a.length();
    int bLen = b.length();

    if (aLen > bLen) {
        addZeros(b, aLen - bLen);
    }
    else if (bLen > aLen) {
        addZeros(a, bLen - aLen);
    }

    int len = max(aLen, bLen);

    string res = "";
    for (int i = 0; i < len; i++) {
        if (a[i] == b[i])
            res += "0";
        else
            res += "1";
    }

    return res;
}

int main()
{
    srand(time(NULL));
	ifstream odczyt;
    fstream zaszyfrowany_tekst;
    odczyt.open("plik.txt", ios::in);

    vector<char> input;
    vector<string> input_porcje;
    vector<string> tekst_zaszyfrowany;
    vector<string> tekst;
    string linia;


//GENEROWANIE KLUCZA
    string key ;
    string first_vector;
    key = generacja();
    first_vector = generacja();


    cout<<"KLUCZ:\t\t"<<key<<endl;
    cout<<"Pierwszy vector: "<<first_vector<<"\n\n";


	if(!odczyt.is_open()) cout<<("Nie udalo sie otworzyc  pliku");

        while(getline(odczyt, linia)){
            for(int i = 0; i < linia.size(); i++){
                input.push_back(linia[i]);

                }}
		odczyt.close();
        string porcja;
//DZIELENIE ZNAKOW PO 8 I DOPELNIANIE ZERAMI

        int textSize = input.size();
        int zerosCount = 8 - (textSize % 8);
        if(textSize%8 != 0){
        for(int i = 0; i < zerosCount; i++){
            input.push_back('0');
        }}

		for(int i =0 ; i<input.size(); i++)
		{
		    porcja += input[i];

		    if((i+1)%8 == 0 ){
            input_porcje.push_back(porcja);
            porcja = "";
            }
		}


		for(int i = 0; i < input_porcje.size(); i++){
            cout << "Linia " << i << " " << input_porcje[i] << endl;
		}

//ZAMIANA NA STRING BINARY

        cout<<endl<<"wiadomosc binarnie"<<endl;

		for( int i = 0 ; i<input_porcje.size() ; i++){

            cout<<"linia "<<i<<" "<<(TextToBinaryString(input_porcje[i]))<<endl;
           tekst.push_back(TextToBinaryString(input_porcje[i]));}

 //SZYFROWANIE

string blok_po_vectorze;
string blok_po_kluczu;


for( int i =0 ; i< tekst.size() ; i++){


        blok_po_kluczu = getXOR(key, first_vector);
        blok_po_vectorze = getXOR(tekst[i] , blok_po_kluczu);
        tekst_zaszyfrowany.push_back(blok_po_vectorze);
        first_vector = blok_po_vectorze;


}



     cout<<endl<<"wiadomosc zaszyfrowana"<<endl;

     for(int i =0 ; i<tekst_zaszyfrowany.size(); i++){
        cout<<tekst_zaszyfrowany[i]<<endl;
     }


    zaszyfrowany_tekst.open("zaszyfrowany_tekst_CFB.txt", ios::out);

    for(int i =0; i<tekst_zaszyfrowany.size() ; i++){


 istringstream in(tekst_zaszyfrowany[i]);
    bitset<8> bin_to_string;
    while(in >> bin_to_string )
       zaszyfrowany_tekst<< char(bin_to_string.to_ulong());}

    zaszyfrowany_tekst.close();

    cout<<endl<<"wiadomosc zaszyfrowana przekonwertowana na typ string zapisana w pliku zaszyfrowany_tekst_CFB.txt";


        cin.get();


    return 0;
}
